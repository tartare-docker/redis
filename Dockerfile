FROM alpine

MAINTAINER Didier FABERT <didier.fabert@gmail.com>
    
RUN apk add --no-cache --update redis && \
    rm -f /var/cache/apk/*

RUN sed -i \
    -e 's/^daemonize yes/daemonize no/' \
    -e 's/^bind 127.0.0.1/bind 0.0.0.0/' \
    -e 's/^# unixsocket /unixsocket /' \
    -e 's/^# unixsocketperm 700/unixsocketperm 777/' \
    -e 's/^logfile \/var\/log\/redis\/redis.log/logfile ""/' \
    /etc/redis.conf

VOLUME [ "/var/lib/redis" ]

USER redis

EXPOSE 6379

ENTRYPOINT [ "/usr/bin/redis-server", "/etc/redis.conf", "--daemonize", "no" ]
